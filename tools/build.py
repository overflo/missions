#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

import argparse
import os
import subprocess
import sys
import textwrap

def build_pbo(path, output_dir):
    print('Building {}...'.format(path), end='')
    sys.stdout.flush()

    args = ['makepbo', '-P', path]
    if output_dir:
        args.append(output_dir)
    proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    (stdout_data, stderr_data) = proc.communicate()
    retval = proc.wait()

    if retval != 0:
        print(textwrap.dedent('''
            {delimiter}
            ERROR: {path}
            {delimiter}
            {message}
            {delimiter}
            ''').format(
                delimiter='=' * 80,
                message = stdout_data,
                path = path
        ))
    else:
        print(' OK!')

    return retval

def build_pbos(args):
    retval = 0

    for entry in os.listdir(args.directory):
        path = os.path.join(args.directory, entry)

        if not os.path.isdir(path):
            continue

        # Exclude prefix
        if any(entry.startswith(exclude_prefix) for exclude_prefix in args.prefix_exclude):
            continue

        if not any(entry.startswith(include_prefix) for include_prefix in args.prefix_directory):
            continue

        if build_pbo(path, args.output_directory) != 0:
            retval = 1

    return retval

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build missions and check if those missions are valid')

    parser.add_argument('directory', help='Directory containing missions directories')
    parser.add_argument('-p', '--prefix-directory', default=['FL_'],
                        help='Make missions only starting with this prefix', action='append')
    parser.add_argument('-x', '--prefix-exclude', default=[],
                        help='Make missions only starting with this prefix', action='append')
    parser.add_argument('-o', '--output-directory', default=None,
                        help='Create pbo files in the given directory')

    args = parser.parse_args()
    sys.exit(build_pbos(args))

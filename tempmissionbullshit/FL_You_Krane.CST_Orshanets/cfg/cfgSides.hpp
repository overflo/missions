class Sides {
    class West {
        name = "USA";
        playerClass = "B_Soldier_F";
        flag = "\rhsusf\addons\rhsusf_main\data\armylogo.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
        color[] = {0, 0.3, 0.8, 1};
        squadRallyPointObject = "FRL_Backpacks_West";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};

    };

    class East : West {
        name = "RU";
        playerClass = "O_Soldier_F";
        flag = "\rhsafrf\addons\rhs_main\data\icons\msv.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
        color[] = {0.5, 0, 0, 1};
        squadRallyPointObject = "FRL_Backpacks_East";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    };
};

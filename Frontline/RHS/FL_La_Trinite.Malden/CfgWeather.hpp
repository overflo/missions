class CfgWeather {
	overcastRandomize = 0;
	overcastFrame[] = {0.45,1}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,0.3}; // [0,1]

	fogRandomize = 0;
	fogFrame[] = {0.025,0.15}; // [0,1]

	timeRandomize = 0;
	timeFrame[] = {7, 18}; // [0,24]
};

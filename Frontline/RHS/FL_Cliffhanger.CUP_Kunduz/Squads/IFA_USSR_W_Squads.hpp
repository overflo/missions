class IFA_USSR_W_SquadSmall {
	alwaysAvailable = 1;
	displayName = "Infantry";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf.paa";
	side = 0;
	scope = 2;
	maxSize = 9;
	availableAt = -1;
	roles[] = {
		{"IFA_USSR_SquadLeader", 1},
		{"IFA_USSR_Rifleman",-1},
		{"IFA_USSR_Medic", 2},
		{"IFA_USSR_LAT", 4},
		{"IFA_USSR_MG", 6},
		{"IFA_USSR_MGAssistant", 6},
		{"IFA_USSR_SMG", 8}
	};
};
class IFA_USSR_W_Sniper {
	alwaysAvailable = 0;
	displayName = "Sniper";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf_snpr.paa";
	side = 0;
	scope = 2;
	maxSize = 1;
	availableAt = -1;
	availableAtArray[] = {25};
	roles[] = {
		{"IFA_USSR_Sniper", 1}
	};
};
class IFA_USSR_W_HMGSquad {
	alwaysAvailable = 0;
	displayName = "HMG Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_hmg.paa";
	side = 0;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {20,40};
	roles[] = {
		{"IFA_USSR_HMG", 1},
		{"IFA_USSR_HMGAssistant",2},
		{"IFA_USSR_MGAmmo", 2}
	};
};
class IFA_USSR_W_HATSquad {
	alwaysAvailable = 0;
	displayName = "AT Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_atk_rl.paa";
	side = 0;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {15,30};
	roles[] = {
		{"IFA_USSR_HAT", 1},
		{"IFA_USSR_HAT_Ammo",2},
		{"IFA_USSR_HAT_Ammo", 2}
	};
};
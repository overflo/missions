class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0.5,0.9}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 0;
	fogFrame[] = {0.1,0.6};

	timeRandomize = 1;
	timeFrame[] = {6, 6.5}; // [0,24]
};

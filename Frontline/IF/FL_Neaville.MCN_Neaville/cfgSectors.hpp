class sector {
    bleedValue[] = {15,15}; // Frontline
    captureTime[] = {120,250};
    firstCaptureTime[] = {90,180};
    sectortype = "normal";

    dependency[] = {};
    ticketValue = 0;
    minUnits = 1;
    isLastSector = "";
};

class CfgSectors {

    class base_west : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

    class base_east : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

    class CfgSectorPath {
        class path_0 {
            /*class sector_0 : sector {
                dependency[] = {"base_west","sector_1"};
                designator = "A";
            };*/

            class sector_1 : sector {
                dependency[] = {"base_west","sector_2"};
                designator = "A";
            };

            class sector_2 : sector {
                dependency[] = {"sector_1","sector_3"};
                designator = "B";
            };

            class sector_3 : sector {
                dependency[] = {"sector_2","sector_4"};
                designator = "C";
            };

			      class sector_4 : sector {
                dependency[] = {"sector_3","sector_5"};
                designator = "D";
            };

			      class sector_5 : sector {
                dependency[] = {"sector_4","sector_6"};
                designator = "E";
            };

			      class sector_6 : sector {
                dependency[] = {"sector_5","base_east"};
                designator = "F";
            };
        };
        class path_1 {

            class sector_7 : sector {
                dependency[] = {"base_west","sector_8"};
                designator = "A";
            };

            class sector_8 : sector {
                dependency[] = {"sector_7","sector_9"};
                designator = "B";
            };

            class sector_9 : sector {
                dependency[] = {"sector_8","sector_10"};
                designator = "C";
            };

			      class sector_10 : sector {
                dependency[] = {"sector_9","sector_11"};
                designator = "D";
            };

			      class sector_11 : sector {
                dependency[] = {"sector_10","sector_12"};
                designator = "E";
            };

			      class sector_12 : sector {
                dependency[] = {"sector_11","base_east"};
                designator = "F";
            };
        };
        class path_2 {

            class sector_13 : sector {
                dependency[] = {"base_west","sector_14"};
                designator = "A";
            };

            class sector_14 : sector {
                dependency[] = {"sector_13","sector_15"};
                designator = "B";
            };

            class sector_15 : sector {
                dependency[] = {"sector_14","sector_16"};
                designator = "C";
            };

			class sector_16 : sector {
                dependency[] = {"sector_15","sector_17"};
                designator = "D";
            };

			class sector_17 : sector {
                dependency[] = {"sector_16","sector_18"};
                designator = "E";
            };

			class sector_18 : sector {
                dependency[] = {"sector_17","base_east"};
                designator = "F";
            };
        };
    };
};

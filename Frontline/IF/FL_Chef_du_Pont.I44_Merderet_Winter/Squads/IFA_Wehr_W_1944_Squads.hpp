class IFA_Wehr_W_SquadSmall {
	alwaysAvailable = 1;
	displayName = "Infantry";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf.paa";
	side = 1;
	scope = 2;
	maxSize = 8;
	availableAt = -1;
	roles[] = {
		{"IFA_Wehr_SquadLeader", 1},
		{"IFA_Wehr_Rifleman",-1},
		{"IFA_Wehr_Medic", 2},
		{"IFA_Wehr_LAT", 4},
		{"IFA_Wehr_OberSchutze", 5},
		{"IFA_Wehr_MG", 6},
		{"IFA_Wehr_MGAssistant", 6},
		{"IFA_Wehr_CorporalSMG", 8}
	};
};
class IFA_Wehr_W_Sniper {
	alwaysAvailable = 0;
	displayName = "Sniper";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf_snpr.paa";
	side = 1;
	scope = 2;
	maxSize = 1;
	availableAt = -1;
	availableAtArray[] = {25};
	roles[] = {
		{"IFA_Wehr_Sniper", 1}
	};
};
class IFA_Wehr_W_HMGSquad {
	alwaysAvailable = 0;
	displayName = "HMG Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_hmg.paa";
	side = 1;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {20,40};
	roles[] = {
		{"IFA_Wehr_MG", 1},
		{"IFA_Wehr_MGAssistant",2},
		{"IFA_Wehr_MGAmmo", 2}
	};
};
class IFA_Wehr_W_HATSquad {
	alwaysAvailable = 0;
	displayName = "AT Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_atk_rl.paa";
	side = 1;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {15,30};
	roles[] = {
		{"IFA_Wehr_HAT", 1},
		{"IFA_Wehr_HAT_Ammo",2},
		{"IFA_Wehr_HAT_Ammo", 2}
	};
};
class IFA_Wehr_W_Stormtroopers {
	alwaysAvailable = 0;
	displayName = "Stormtroopers";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_gmg.paa";
	side = 1;
	scope = 2;
	maxSize = 6;
	availableAt = -1;
	availableAtArray[] = {20,40};
	roles[] = {
		{"IFA_Wehr_Storm_SquadLeader", 1},
		{"IFA_Wehr_Storm_Medic", 1},
		{"IFA_Wehr_Storm_Rifleman", 1},
		{"IFA_Wehr_Storm_LAT", -1},
		{"IFA_Wehr_Storm_STG", -1}
	};
};
class CfgWeather {
	overcastRandomize = 0;
	overcastFrame[] = {0,0.2}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,0}; // [0,1]

	fogRandomize = 0;
	fogFrame[] = {0.4,0.4}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {0, 24}; // [0,24]			0, 7
};

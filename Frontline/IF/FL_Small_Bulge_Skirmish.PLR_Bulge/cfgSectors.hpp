class sector {
    bleedValue[] = {4,4}; // Frontline
    captureTime[] = {90,180};
    firstCaptureTime[] = {60,120};
    sectortype = "normal";

    dependency[] = {};
    ticketValue = 0;
    minUnits = 1;
    isLastSector = "";
};

class CfgSectors {

    class base_west : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

    class base_east : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

	class CfgSectorPath {
        class path_0 {
            class sector_1 : sector {
                dependency[] = {"base_west","sector_2"};
                designator = "A";
            };

            class sector_2 : sector {
                dependency[] = {"sector_1","sector_3"};
                designator = "B";
            };

            class sector_3 : sector {
                dependency[] = {"sector_2","sector_4"};
                designator = "C";
            };

			class sector_4 : sector {
                dependency[] = {"sector_3","base_east"};
                designator = "D";
            };
        };

        class path_1 {
            class sector_5 : sector {
                dependency[] = {"base_west","sector_6"};
                designator = "A";
            };

            class sector_6 : sector {
                dependency[] = {"sector_5","sector_7"};
                designator = "B";
            };

            class sector_7 : sector {
                dependency[] = {"sector_6","sector_8"};
                designator = "C";
            };

			class sector_8 : sector {
                dependency[] = {"sector_7","sector_9"};
                designator = "D";
            };
			
			class sector_9 : sector {
                dependency[] = {"sector_8","base_east"};
                designator = "E";
			};	
        };

        class path_2 {

            class sector_10 : sector {
                dependency[] = {"base_west","sector_11"};
                designator = "A";
            };

            class sector_11 : sector {
                dependency[] = {"sector_10","sector_12"};
                designator = "B";
            };

            class sector_12 : sector {
                dependency[] = {"sector_11","sector_13"};
                designator = "C";
            };

			class sector_13 : sector {
                dependency[] = {"sector_12","base_east"};
                designator = "D";
            };
        };

        class path_3 {

            class sector_14 : sector {
                dependency[] = {"base_west","sector_15"};
                designator = "A";
            };

            class sector_15 : sector {
                dependency[] = {"sector_14","sector_16"};
                designator = "B";
            };

            class sector_16 : sector {
                dependency[] = {"sector_15","sector_17"};
                designator = "C";
            };
			
            class sector_17 : sector {
                dependency[] = {"sector_16","sector_18"};
                designator = "D";
            };

			class sector_18 : sector {
                dependency[] = {"sector_17","base_east"};
                designator = "E";
            };
        };
    };
};
class IFA_USSR_Winter_W_SquadSmall {
	alwaysAvailable = 1;
	displayName = "Infantry";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf.paa";
	side = 0;
	scope = 2;
	maxSize = 9;
	availableAt = -1;
	roles[] = {
		{"IFA_USSR_Winter_SquadLeader", 1},
		{"IFA_USSR_Winter_Rifleman",-1},
		{"IFA_USSR_Winter_Medic", 2},
		{"IFA_USSR_Winter_LAT", 4},
		{"IFA_USSR_Winter_MG", 6},
		{"IFA_USSR_Winter_MGAssistant", 6},
		{"IFA_USSR_Winter_HAT", 8},
		{"IFA_USSR_Winter_SMG", 8}
	};
};
class IFA_USSR_Winter_W_Sniper {
	alwaysAvailable = 0;
	displayName = "Sniper";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf_snpr.paa";
	side = 0;
	scope = 2;
	maxSize = 1;
	availableAt = -1;
	availableAtArray[] = {25};
	roles[] = {
		{"IFA_USSR_Winter_Sniper", 1}
	};
};
class IFA_USSR_Winter_W_HMGSquad {
	alwaysAvailable = 0;
	displayName = "HMG Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_hmg.paa";
	side = 0;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {20,40};
	roles[] = {
		{"IFA_USSR_Winter_HMG", 1},
		{"IFA_USSR_Winter_HMGAssistant",2},
		{"IFA_USSR_Winter_MGAmmo", 2}
	};
};
class IFA_USSR_Winter_W_HATSquad {
	alwaysAvailable = 0;
	displayName = "AT Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_atk_rl.paa";
	side = 0;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {15,30};
	roles[] = {
		{"IFA_USSR_Winter_HAT", 1},
		{"IFA_USSR_Winter_HAT_Ammo",2},
		{"IFA_USSR_Winter_HAT_Ammo_1", 2}
	};
};
class IFA_USSR_W_PartisanSquad {
	alwaysAvailable = 0;
	displayName = "Partisans";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_civ.paa";
	side = 0;
	scope = 2;
	maxSize = 5;
	availableAt = -1;
	availableAtArray[] = {20,40};
	roles[] = {
		{"IFA_USSR_PART_SquadLeader", 1},
		{"IFA_USSR_PART_Medic", 1},
		{"IFA_USSR_PART_LMG", 3},
		{"IFA_USSR_PART_SMG", -1},
		{"IFA_USSR_PART_LAT", -1}
	};
};
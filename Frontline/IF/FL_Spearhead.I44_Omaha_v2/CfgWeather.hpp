class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0.1,0.6}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 1;
	fogFrame[] = {0,0.1}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {3, 19}; // [0,24]
};

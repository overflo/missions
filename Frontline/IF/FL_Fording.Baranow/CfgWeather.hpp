class CfgWeather {
	overcastRandomize = 0;
	overcastFrame[] = {0.8,1}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,0.6}; // [0,1]

	fogRandomize = 0;
	fogFrame[] = {1,1}; // [0,1]

	timeRandomize = 0;
	timeFrame[] = {4, 18}; // [0,24]
};

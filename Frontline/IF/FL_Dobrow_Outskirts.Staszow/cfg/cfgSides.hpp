class Sides {
    class West {
        name = "Wehrmacht";
        playerClass = "LIB_GER_rifleman";
        flag = "\WW2\Core_t\Data_t\Factions\Wehrmacht.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
        color[] = {0, 0.3, 0.8, 1};
        squadRallyPointObject = "FRL_Rally_Wehrmacht";
        FOBObjects[] = {{"LIB_AmmoCrates_NoInteractive_Large", {0,0,0}, 0}, {"Land_GerRadio", {0,0.22,0.62}, 177}};

    };

    class East : West {
        name = "Red Army";
        playerClass = "LIB_SOV_rifleman";
        flag = "\WW2\Core_t\Data_t\Factions\RKKA.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
        color[] = {0.5, 0, 0, 1};
        squadRallyPointObject = "FRL_Backpacks_East";
        FOBObjects[] = {{"LIB_AmmoCrates_NoInteractive_Large", {0,0,0}, 0}, {"Vysilacka", {0,-0.28,0.81}, 177}};
    };
};

class sector {
    bleedValue[] = {15,15}; // Frontline
    captureTime[] = {90,180};
    firstCaptureTime[] = {60,120};
    sectortype = "normal";

    dependency[] = {};
    ticketValue = 0;
    minUnits = 1;
    isLastSector = "";
};

class CfgSectors {

    class base_west : sector {
        designator = "HQ";
        sectortype = "mainbase";
    };

    class base_east : sector {
        designator = "HQ";
        sectortype = "mainbase";
    };

    class CfgSectorPath {
        class path_0 {
            class sector_0 : sector {
                dependency[] = {"base_west","sector_1"};
                designator = "A";
            };

            class sector_1 : sector {
                dependency[] = {"sector_0","sector_2"};
                designator = "B";
            };

            class sector_2 : sector {
                dependency[] = {"sector_1","sector_3"};
                designator = "C";
            };

		        class sector_3: sector {
                dependency[] = {"sector_2","sector_3"};
                designator = "D";
            };

		        class sector_4: sector {
                dependency[] = {"sector_3","base_east"};
                designator = "E";
            };
        };


        class path_1 {
            class sector_5: sector {
                dependency[] = {"base_west","sector_6"};
                designator = "A";
            };

            class sector_6: sector {
                dependency[] = {"sector_5","sector_7"};
                designator = "B";
            };

            class sector_7: sector {
                dependency[] = {"sector_6","sector_8"};
                designator = "C";
            };

            class sector_8: sector {
                dependency[] = {"sector_7","sector_9"};
                designator = "D";
            };

		        class sector_9: sector {
                dependency[] = {"sector_8","sector_10"};
                designator = "E";
            };

		        class sector_10 : sector {
                dependency[] = {"sector_9","base_east"};
                designator = "F";
            };
        };
    };
};

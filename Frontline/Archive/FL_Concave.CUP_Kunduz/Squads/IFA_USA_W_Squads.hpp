class IFA_USA_W_SquadSmall {
	alwaysAvailable = 1;
	displayName = "Infantry";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf.paa";
	side = 0;
	scope = 2;
	maxSize = 10;
	availableAt = -1;
	roles[] = {
		{"IFA_USA_SquadLeader", 1},
		{"IFA_USA_Rifleman",-1},
		{"IFA_USA_Medic", 2},
		{"IFA_USA_Engineer", 4},
		{"IFA_USA_MG", 5}
	};
};
class IFA_USA_W_Sniper {
	alwaysAvailable = 0;
	displayName = "Sniper";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf_snpr.paa";
	side = 0;
	scope = 2;
	maxSize = 1;
	availableAt = -1;
	availableAtArray[] = {25};
	roles[] = {
		{"IFA_USA_Sniper", 1}
	};
};
/*class IFA_USA_W_HMGSquad {
	alwaysAvailable = 0;
	displayName = "HMG Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_hmg.paa";
	side = 0;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {20,40};
	roles[] = {
		{"IFA_USA_M1919A2_Diabolicals_Amazing_MG", 1},
		{"IFA_USA_MGAssistant",2},
		{"IFA_USA_MGAmmo", 2}
	};
};
*/
class IFA_USA_W_HATSquad {
	alwaysAvailable = 0;
	displayName = "AT Team";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_atk_rl.paa";
	side = 0;
	scope = 2;
	maxSize = 3;
	availableAt = -1;
	availableAtArray[] = {15,30};
	roles[] = {
		{"IFA_USA_HAT", 1},
		{"IFA_USA_HAT_Ammo",2},
		{"IFA_USA_HAT_Ammo", 2}
	};
};
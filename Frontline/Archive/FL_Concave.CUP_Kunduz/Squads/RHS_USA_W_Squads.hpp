class RHS_USA_W_SquadSmall {
	alwaysAvailable = 1;
	displayName = "Light Inf";
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf.paa";
	side = 1;
	scope = 2;
	maxSize = 6;
	availableAt = -1;
	roles[] = {
		{"RHS_USA_W_SquadLeader", 1},
		{"RHS_USA_W_Rifleman",-1},
		{"RHS_USA_W_Medic", 2},
		{"RHS_USA_W_LAT", 5},
		{"RHS_USA_W_AR", 5}
	};
};

class RHS_USA_W_Squad1: RHS_USA_W_SquadSmall {
	displayName = "Regular (Grenadier)";
	maxSize = 9;
	availableAt = -1;
	roles[] = {
		{"RHS_USA_W_SquadLeader", 1},
		{"RHS_USA_W_Rifleman",-1},
		{"RHS_USA_W_Medic", 2},
		{"RHS_USA_W_AR", 4},
		{"RHS_USA_W_Grenadier", 5},
		{"RHS_USA_W_LAT", 5},
		{"RHS_USA_W_Engineer", 5}
	};
};

class RHS_USA_W_Squad2: RHS_USA_W_Squad1 {
	displayName = "Regular (Marksman)";
	availableAt = -1;
	availableAtArray[] = {15};
	roles[] = {
		{"RHS_USA_W_SquadLeader", 1},
		{"RHS_USA_W_Rifleman",-1},
		{"RHS_USA_W_Medic", 2},
		{"RHS_USA_W_AR", 4},
		{"RHS_USA_W_LAT", 5},
		{"RHS_USA_W_Engineer", 5},
		{"RHS_USA_W_Marksman", 8}
	};
};

class RHS_USA_W_Squad3: RHS_USA_W_Squad1 {
	displayName = "Regular (Anti-Tank)";
	availableAt = 20;
	roles[] = {
		{"RHS_USA_W_SquadLeader", 1},
		{"RHS_USA_W_Rifleman",-1},
		{"RHS_USA_W_Medic", 2},
		{"RHS_USA_W_AR", 4},
		{"RHS_USA_W_Engineer", 5},
		{"RHS_USA_W_HAT", 8}
	};
};

class RHS_USA_W_SquadMG {
	displayName = "MG Team";
	side = 1;
	scope = 2;
	maxSize = 3;
	mapIcon = "\pr\frl\addons\clients\markers\blufor\frl_b_inf_mg.paa";
	availableAt = 15;
	roles[] = {
		{"RHS_USA_W_TeamLeader_MG", 1},
		{"RHS_USA_W_MG", 2},
		{"RHS_USA_W_Rifleman", -1}
	};
};
